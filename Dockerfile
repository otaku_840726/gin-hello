# syntax=docker/dockerfile:1

##
## Build
##
FROM golang:1.16-buster AS build

WORKDIR /app

COPY ./ ./

RUN go build -v -o /bin/gin-hello ./cmd

##
## Deploy
##
FROM gcr.io/distroless/base-debian10

COPY --from=build /bin/gin-hello /opt/gin-hello

EXPOSE 80

WORKDIR /opt

USER nonroot:nonroot

ENTRYPOINT ["./gin-hello"]
