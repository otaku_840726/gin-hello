package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {

	r := gin.Default()

	data := gin.H{
		"code": "0",
		"msg":  "尼好",
	}

	r.GET("/jsonp", func(c *gin.Context) {
		c.JSONP(http.StatusOK, data)
	})

	r.GET("/asciiJSON", func(c *gin.Context) {
		c.AsciiJSON(http.StatusOK, data)
	})

	r.GET("/purejson", func(c *gin.Context) {
		c.PureJSON(http.StatusOK, data)
	})

	r.GET("/json", func(c *gin.Context) {
		c.JSON(http.StatusOK, data)
	})

	r.GET("/YAML", func(c *gin.Context) {
		c.YAML(http.StatusOK, data)
	})

	_ = r.Run(":80")
}
